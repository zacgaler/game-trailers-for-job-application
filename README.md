- # Game Trailers for Job Application

This is a public list of the trailers for games I have been involved in creating! The links to the trailers are included in the README.


**- MineGun**
- Trailer: https://youtu.be/wbswaqC77jQ



**- CryoSleep**
-  Trailer is saved as an MP4 file in this project.



**- Spektacles**
- Trailer: https://youtu.be/7hcsAourIZ8
- Itch.IO Page: https://spektaculargames.itch.io/spektacles



**- Loki**
- Trailer: https://youtu.be/JrZ6a4TUFvY
- Itch.IO Page: https://omoriarty.itch.io/loki
